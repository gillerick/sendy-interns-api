import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})

//Fetches all interns
Route.get('/interns', 'InternsController.index')

//Fetches a single intern
Route.get('/interns/:id', 'InternsController.show')
