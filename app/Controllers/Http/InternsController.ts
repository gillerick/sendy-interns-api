// import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
'use strict'

import Intern from "App/Models/Intern";


export default class InternsController {
    async index() {
        const interns = await Intern
            .query()
            .preload('stacks', (query) => {
                query.pivotColumns(['intern_id'])
            })

        interns.forEach((intern) => {
            intern.stacks.forEach((stack) => {
                console.log(stack.$extras.pivot_intern_id)
                console.log(stack.$extras.pivot_stack_id)
                console.log(stack.$extras.pivot_created_at)
            })
        })
        return interns;
    }

    async show({ response, params: { id } }) {

        const intern = await Intern.find(id);

        if (intern) {
            const stacks = await intern.related('stacks').query()
            response.header('Content-Type', 'application.json')
            return response.json({
                message: "Interns Successfully Fetched",
                data:{intern, stacks}
            })



        } else {
            return response.status(404).json({
                message: "Intern not found"

            })
        }


    }




}

module.exports = InternsController
