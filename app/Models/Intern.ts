import { DateTime } from 'luxon'
import { BaseModel, column, ManyToMany, manyToMany, } from '@ioc:Adonis/Lucid/Orm'
import Stack from "App/Models/Stack";
export default class Intern extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public email: string

  @column()
  public image: string

  @column()
  public title: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @manyToMany(() => Stack, {
    localKey: 'id',
    pivotForeignKey: 'intern_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'stack_id',
    pivotTable: 'intern_stack',

  })
  public stacks: ManyToMany<typeof Stack>
}
